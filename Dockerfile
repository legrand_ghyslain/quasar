FROM harisekhon/ubuntu-java

# Install curl, git, unzip, software-properties-common, python, nodejs, cordova
RUN \
apt-get update && \
apt-get install -y software-properties-common curl python git unzip && \
curl https://nodejs.org/dist/v8.11.3/node-v8.11.3-linux-x64.tar.gz | tar xz -C /usr/local/ --strip=1 && \
npm install -g cordova && npm install -g @quasar/cli

# Specify useful environment variables
RUN echo $JAVA_HOME
ENV GRADLE_HOME /usr/local/gradle
ENV GRADLE_VERSION 4.4
ENV PATH ${GRADLE_HOME}/bin:${JAVA_HOME}/bin:${ANDROID_HOME}/tools:$ANDROID_HOME/platform-tools:$PATH
ENV ANDROID_HOME /usr/local/android-sdk-linux
ENV ANDROID_SDK_ROOT /usr/local/android-sdk-linux

# Install Gradle
RUN mkdir -p ${GRADLE_HOME} && \
  curl -L https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip > /tmp/gradle.zip && \
  unzip /tmp/gradle.zip -d ${GRADLE_HOME} && \
  mv ${GRADLE_HOME}/gradle-${GRADLE_VERSION}/* ${GRADLE_HOME} && \
  rm -r ${GRADLE_HOME}/gradle-${GRADLE_VERSION}/

# Install Android SDK
RUN curl https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip > sdk-tools-linux-4333796.zip
RUN unzip sdk-tools-linux-4333796.zip -d ${ANDROID_HOME}

# Add ANDROID_HOME to PATH
ENV PATH $PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

# Update android-sdk
RUN ( sleep 5 && while [ 1 ]; do sleep 1; echo y; done ) | /usr/local/android-sdk-linux/tools/bin/sdkmanager "platform-tools" "platforms;android-28" "build-tools;28.0.3"

RUN mkdir /app
#COPY . /app
WORKDIR /app
#RUN npm i

#RUN cordova telemetry on
#RUN quasar mode add cordova
#RUN quasar mode add electron

#WORKDIR /app/src-cordova
#RUN cordova requirements
#WORKDIR /app
#
#RUN quasar build -m cordova -T android
#RUN quasar build -m electron
#RUN quasar build

