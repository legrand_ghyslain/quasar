
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'wifi', component: () => import('pages/Wifi.vue') },
      { path: 'files', component: () => import('pages/Files.vue') },
      { path: 'ftp', component: () => import('pages/Ftp.vue') },
      { path: 'upnp', component: () => import('pages/Upnp.vue') },
      { path: 'mdns', component: () => import('pages/Mdns.vue') },
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
